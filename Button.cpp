#include "Button.hpp"

Button::Button(const sf::Vector2f& size, float r, uint p){
    this->radius = r;
    this->cornerPoints = p;
    this->size = size;
    update();
}

Button::~Button() {}

void Button::setSize(const sf::Vector2f& size) {
    this->size = size;
    update();
}

void Button::setCornersRadius(float radius) {
    this->radius = radius;
    update();
}

const sf::Vector2f& Button::getSize() const { return this->size; }

float Button::getCornersRadius() const { return this->radius; }

void Button::setCornerPointCount(unsigned int count) { this->cornerPoints = count; }

std::size_t Button::getPointCount() const { return this->cornerPoints*4; }

sf::Vector2f Button::getPoint(std::size_t index) const {
    if(index >= this->cornerPoints*4)
        return sf::Vector2f(0,0);

    float deltaAngle = 90.0f/(this->cornerPoints-1);
    sf::Vector2f center;
    unsigned int centerIndex = index/this->cornerPoints;

    switch(centerIndex)
    {
        case 0: center.x = this->getSize().x - this->radius; center.y = this->radius; break;
        case 1: center.x = this->radius; center.y = this->radius; break;
        case 2: center.x = this->radius; center.y = this->getSize().y - this->radius; break;
        case 3: center.x = this->getSize().x - this->radius; center.y = this->getSize().y - this->radius; break;
    }
    return sf::Vector2f(this->radius*cos(deltaAngle*(index-centerIndex)*M_PI/180)+center.x,
                        -this->radius*sin(deltaAngle*(index-centerIndex)*M_PI/180)+center.y);
}

void Button::check(sf::Event et) {
    auto it = this->supported_events.equal_range(et.type);
    for (auto i = it.first; i != it.second; ++i) {
        if (this->visible) {
            if (i->first == sf::Event::MouseButtonPressed) {
                if (i->second.ev.button == et.mouseButton.button) {
                    if (this->getGlobalBounds().contains(et.mouseButton.x, et.mouseButton.y)) {
                        i->second.f();
                        return;
                    }
                }
            }
        }
    }
}
