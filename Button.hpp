#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <cmath>
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Shape.hpp>

#include "GUITarget.hpp"

class Button : public sf::Shape, public GUITarget
{
private:
    float radius;
    uint cornerPoints;
    sf::Vector2f size;
public:
    Button(const sf::Vector2f& size = sf::Vector2f(0, 0), float r = 0, uint p = 2);
    ~Button();

    void setSize(const sf::Vector2f& size);
    const sf::Vector2f& getSize() const;
    void setCornerPointCount(unsigned int count);
    void setCornersRadius(float radius);
    float getCornersRadius() const;

    virtual std::size_t getPointCount() const;
    virtual sf::Vector2f getPoint(std::size_t index) const;
    virtual void check(sf::Event et);
};

#endif