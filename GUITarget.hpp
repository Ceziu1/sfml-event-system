#ifndef GUITARGET_HPP
#define GUITARGET_HPP

#include <SFML/Graphics.hpp>
#include <iostream>

#include "Visibility.hpp"

class GUITarget
{
public:
    union Events;
    struct Exec;
protected:
    unsigned int z_index;
    bool visible;
    std::multimap<sf::Event::EventType, GUITarget::Exec> supported_events;
public:
    GUITarget();
    ~GUITarget();
    bool visibility();
    void visibility(Visibility v);
    void addEventListener(sf::Event::EventType et, GUITarget::Events e, void (*p)(void));
    virtual void check(sf::Event et) = 0;

    union Events {
        sf::Keyboard::Key           key;
        sf::Mouse::Button           button;
        sf::Mouse::Wheel            wheel;
    };

    struct Exec {
        GUITarget::Events   ev;
        void                (*f)();
    };
};

#endif