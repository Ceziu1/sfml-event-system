#include <SFML/Graphics.hpp>

//----- USER INCLUDES -----
#include "Visibility.hpp"
#include "Layer.hpp"
#include "Button.hpp"
#include "EventManager.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(1280, 720), "SFML works!");
    window.setFramerateLimit(60);

    EventManager* em = EventManager::getInstance();

    Layer::_Items itm;
    sf::CircleShape circ1(50), circ2(50);
    circ1.setFillColor(sf::Color::Cyan); circ2.setFillColor(sf::Color::Green);
    circ1.setPosition(50, 50); circ2.setPosition(200, 600);
    Layer l("MAIN"); l.debugging(true); em+= l;
    itm.background = &circ1; l.add(itm);
    itm.background = &circ2; l.add(itm);

    Button b1(sf::Vector2f(200,100), 30, 5); b1.setFillColor(sf::Color::Magenta);
    b1.setPosition(500,200); b1.setSize(sf::Vector2f(200, 200)); itm.btn_interactive = &b1;
    GUITarget::Events ev; ev.button = sf::Mouse::Button::Left;
    b1.addEventListener(sf::Event::MouseButtonPressed, ev, [](){
        std::cout<<"LAMBDA"<<std::endl;
    }); l.add(itm, true);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            em->check(event);
        }

        window.clear();
        em->draw(window);
        window.display();
    }
    return 0;
}
