#ifndef LAYER_HPP
#define LAYER_HPP

#include <SFML/Graphics.hpp>
#include <list>

#include "Visibility.hpp"
#include "GUITarget.hpp"
#include "Button.hpp"

class Layer : sf::Drawable
{
public:
    union _Items {
        sf::Shape *background;
        Button *btn_interactive;
    };

    Layer(std::string name);
    ~Layer();

    void add(Layer::_Items d, bool interactive = false);
    bool visibility();
    void debugging(bool b);
    void visibility(Visibility v);

    static GUITarget *active_element;
    static unsigned int layer_count;

    friend std::ostream &operator<<(std::ostream &o, Layer &l);

private:
    std::list<Layer::_Items> layer_items;
    sf::Rect<int> layer_borders;

    unsigned int z_layer;
    bool visible;
    bool debug;
    std::string name;
    Layer *child_layer;
    friend class EventManager;

    void _recalculateBounds();

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;
};

#endif