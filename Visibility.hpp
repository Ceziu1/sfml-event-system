#ifndef VISIBILITY_HPP
#define VISIBILITY_HPP

enum class Visibility {
    VISIBLE,
    HIDDEN
};

#endif