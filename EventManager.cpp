#include "EventManager.hpp"

EventManager* EventManager::getInstance() {
  static EventManager instance;
  return &instance;
}

void EventManager::check(sf::Event et) {
  for(Layer* l: this->layers)
    if (l->visibility() && l->child_layer)
      for (Layer::_Items i : l->child_layer->layer_items)
        i.btn_interactive->check(et);
}

EventManager* operator+=(EventManager* e, Layer& l){
  e->layers.insert(&l);
  return e;
}

void EventManager::draw(sf::RenderWindow& w) {
  for (Layer* i : this->layers)
    i->draw(w, sf::RenderStates::Default);
}

// std::ostream& operator<<(std::ostream& o, EventManager* e) {
//   o<<e->layers.size();
//   return o;
// }
