TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Button.cpp \
        EventManager.cpp \
        GUITarget.cpp \
        Layer.cpp \
        main.cpp

DISTFILES += \
    Event_System.pro.user

HEADERS += \
    Button.hpp \
    EventManager.hpp \
    GUITarget.hpp \
    Layer.hpp \
    Visibility.hpp

unix|win32: LIBS += -lsfml-graphics \
                    -lsfml-audio \
                    -lsfml-window \
                    -lsfml-system
