#include "Layer.hpp"

unsigned int Layer::layer_count = 0;
GUITarget* Layer::active_element = nullptr;

Layer::Layer(std::string name){
    this->layer_borders = sf::Rect<int>(0,0,0,0);
    this->z_layer = Layer::layer_count++;
    this->visible = true;
    this->name = name;
    this->child_layer = nullptr;
}

Layer::~Layer() { Layer::layer_count--; }

void Layer::visibility(Visibility v) {
    v == Visibility::VISIBLE ? this->visible = true : this->visible = false;
}

bool Layer::visibility() { return this->visible; }

void Layer::_recalculateBounds() {
    int min_l = this->layer_borders.left, l;
    int min_t = this->layer_borders.top, t;
    int max_w = this->layer_borders.width, w;
    int max_h = this->layer_borders.height, h;
    for (Layer::_Items i : this->layer_items) {
        if (this->layer_borders.width == 0 || this->layer_borders.height == 0) {
            this->layer_borders = (sf::IntRect)i.background->getGlobalBounds();
            min_l = this->layer_borders.left;
            min_t = this->layer_borders.top;
            max_w = this->layer_borders.width;
            max_h = this->layer_borders.height;
            continue;
        }
        l = i.background->getGlobalBounds().left;
        t = i.background->getGlobalBounds().top;
        w = i.background->getGlobalBounds().width;
        h = i.background->getGlobalBounds().height;
        if (min_l > l) min_l = l;
        if (min_t > t) min_t = t;
        if (max_w < l + w) max_w = l + w;
        if (max_h < t + h) max_h = t + h;
    }
    if (this->child_layer != nullptr){
        for (Layer::_Items i : this->child_layer->layer_items) {
            if (this->layer_borders.width == 0 || this->layer_borders.height == 0) {
                this->layer_borders = (sf::IntRect)i.btn_interactive->getGlobalBounds();
                min_l = this->layer_borders.left;
                min_t = this->layer_borders.top;
                max_w = this->layer_borders.width;
                max_h = this->layer_borders.height;
                continue;
            }
            l = i.btn_interactive->getGlobalBounds().left;
            t = i.btn_interactive->getGlobalBounds().top;
            w = i.btn_interactive->getGlobalBounds().width;
            h = i.btn_interactive->getGlobalBounds().height;
            if (min_l > l) min_l = l;
            if (min_t > t) min_t = t;
            if (max_w < l + w) max_w = l + w;
            if (max_h < t + h) max_h = t + h;
        }}
    this->layer_borders.left = min_l;
    this->layer_borders.top = min_t;
    this->layer_borders.width = max_w - min_l;
    this->layer_borders.height = max_h - min_t;
}

void Layer::add(Layer::_Items d, bool interactive) {
    if (interactive) {
        if (this->child_layer == nullptr)
            this->child_layer = new Layer(this->name + "|child");
        this->child_layer->layer_items.push_back(d);
    } else this->layer_items.push_back(d);
    
    _recalculateBounds();
}

void Layer::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    if (!this->visible) return;
    if (this->debug) {
        sf::RectangleShape r(sf::Vector2f(this->layer_borders.width, this->layer_borders.height));
        r.setPosition(this->layer_borders.left, this->layer_borders.top);
        r.setFillColor(sf::Color(255, 0, 0, 120));
        target.draw(r);
    }
    for (const auto i : this->layer_items)
        target.draw(*i.background);
    if (this->child_layer) {
        GUITarget* ptr;
        for (const auto i : this->child_layer->layer_items) {
            ptr = dynamic_cast<GUITarget*>(i.btn_interactive);
            if (!ptr) std::cout<<"Casting failed"<<std::endl;
            else if(i.btn_interactive->visibility())
                target.draw(*i.btn_interactive);
        }
    }
}

void Layer::debugging(bool b) { this->debug = b; }

std::ostream& operator<<(std::ostream& o, Layer& l){
    o<<"Layer: "<<l.name;
    return o;
}
