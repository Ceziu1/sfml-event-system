#include "GUITarget.hpp"

GUITarget::GUITarget() {
  this->visible = true;
}

GUITarget::~GUITarget() {}

void GUITarget::visibility(Visibility v) {
  v == Visibility::VISIBLE ? this->visible = true : this->visible = false;
}

bool GUITarget::visibility() {
  return this->visible;
}

void GUITarget::addEventListener(sf::Event::EventType et, GUITarget::Events e, void (*p)(void)) {
  this->supported_events.emplace(et, Exec{e, p});
}

// void GUITarget::check(sf::Event et) {
//   auto it = this->supported_events.equal_range(et.type);
//   for (auto i = it.first; i != it.second; ++i) {
//     if (this->visible) {
//       if (i->first == sf::Event::MouseButtonPressed) {
//         if (i->second.ev.button == et.mouseButton.button) {
//           i->second.f();
//           return;
//         }
//       }
//     }
//   }
// }
