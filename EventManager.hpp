#ifndef EVENTMANAGER_HPP
#define EVENTMANAGER_HPP

#include <iostream>
#include <set>

#include "Layer.hpp"

class EventManager
{
private:
    EventManager(){};

    std::set<Layer*> layers;
public:
    void draw(sf::RenderWindow& w);
    void check(sf::Event et);

    friend EventManager* operator+=(EventManager* e,Layer& l);
    friend std::ostream& operator<<(std::ostream& o, EventManager* e);
    
    static EventManager* getInstance();
    
    EventManager(const EventManager&) = delete;
    EventManager& operator=(const EventManager&) = delete;
};

#endif